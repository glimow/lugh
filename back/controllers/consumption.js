const _ = require('lodash');
const dummy = require('../dummy-data.json');

module.exports = {
   '/': {
      get: {
         action(req, res) {
          
            const data = dummy.slice(24*7*3, 24*7*4).map(
               sample => ({ power: sample.power/3, datetime: new Date(sample.datetime)})
            );

            res.json(_.orderBy(data, 'datetime'));
            return;
         },
         level: 'public'
      }
   }
};
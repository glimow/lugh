const _ = require('lodash');
const dummy = require('../dummy-data.json');

module.exports = {
   '/': {
      get: {
         action(req, res) {
            const solar = dummy.slice(24*7, 24*7*2).map(
               sample => ({ power: sample.power/6, datetime: new Date(sample.datetime)})
            );
            const wind = dummy.slice(0, 24*7).map(
               sample => ({ power: sample.power/8, datetime: new Date(sample.datetime)})
            );
            const data = _.zipWith(solar, wind, (solarSample, windSample) => ({
               power: solarSample.power + windSample.power,
               datetime: new Date(solarSample.datetime)
            }));
            res.json(_.orderBy(data, 'datetime'));
            return;
         },
         level: 'public'
      }
   },
   '/solar': {
      get: {
         action(req, res) {
          
            const data = dummy.slice(24*7, 24*7*2).map(
               sample => ({ power: sample.power/6, datetime: new Date(sample.datetime)})
            );

            res.json(_.orderBy(data, 'datetime'));
            return;
         },
         level: 'public'
      }
   },
   '/wind': {
      get: {
         action(req, res) {
          
            const data = dummy.slice(0, 24*7).map(
               sample => ({ power: sample.power/8, datetime: new Date(sample.datetime)})
            );

            res.json(_.orderBy(data, 'datetime'));
            return;
         },
         level: 'public'
      }
   },
};
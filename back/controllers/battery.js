const _ = require('lodash');
const dummy = require('../dummy-data.json');

module.exports = {
   '/': {
      get: {
         action(req, res) {
          
            const data = dummy.slice(24*7*2, 24*7*3).map(
               sample => ({ power: sample.power/100, datetime: new Date(sample.datetime)})
            );

            res.json(_.orderBy(data, 'datetime'));
            return;
         },
         level: 'public'
      }
   }
};
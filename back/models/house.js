import mongoose from 'mongoose';

const BATTERY_MAX_CAPACITY = 1500;

const houseSchema = new mongoose.Schema({
   battery : {
      voltage: 'number',
      percentage: 'number',
   },
   consumption: {
      voltage: 'number',
      intensity: 'number',
   },
   production: {
      solar: {
         voltage: 'number',
         intensity: 'number',
      },
      wind: {
         voltage: 'number',
         intensity: 'number',
      },
   },
   type: { type: 'string', enum: ['GROUND_TRUTH', 'FORECAST'], default: 'GROUND_TRUTH'},
   timestamp: { type: Date, default: Date.now },
},{
   toObject: {
      virtuals: true,
   },
   toJSON: {
      virtuals: true, 
   },
});

houseSchema.virtual('battery.charge').get(function(){
   return BATTERY_MAX_CAPACITY * this.battery.percentage;
});

houseSchema.virtual('consumption.power').get(function(){
   return this.consumption.voltage * this.consumption.intensity;
});

houseSchema.virtual('production.solar.power').get(function(){
   return this.production.solar.voltage * this.production.solar.intensity;
});

houseSchema.virtual('production.wind.power').get(function(){
   return this.production.wind.voltage * this.production.wind.intensity;
});

houseSchema.virtual('production.power').get(function(){
   return this.consumption.solar.power + this.production.wind.power;
});

const House = mongoose.model('house', houseSchema);

export default House;
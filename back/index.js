const express = require("express");
const path = require("path");
const lumie = require("lumie");
const cors = require("cors");

const app = express();

app.use(cors())

lumie.load(app, {
  preURL: "api",
  verbose: true,
  ignore: ["*.spec", "*.action"],
  controllers_path: path.join(__dirname, "controllers")
});

const server = app.listen(3000, "127.0.0.1", () => {
  const { address, port } = server.address();
  console.log("Lugh server listening at http://%s:%s", address, port);
});
/* eslint-disable */
import Vue from 'vue';
import ShardsVue from 'shards-vue';
import VueI18n from 'vue-i18n'

// Locales
import messages from './locales'
console.log(messages);
Vue.use(VueI18n)
// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en', // set locale
  messages, // set locale messages
})

// Styles
import 'bootstrap/dist/css/bootstrap.css';
import '@/scss/shards-dashboards.scss';
import '@/assets/scss/date-range.scss';

// Core
import App from './App.vue';
import router from './router';

// Layouts
import Default from '@/layouts/Default.vue';

ShardsVue.install(Vue);

Vue.component('default-layout', Default);

Vue.config.productionTip = false;
Vue.prototype.$eventHub = new Vue();

new Vue({
  i18n,
  router,
  render: h => h(App),
}).$mount('#app');

export default function () {
  return [{
    title: 'home.energy',
    to: {
      name: 'blog-overview',
    },
    htmlBefore: '<i class="material-icons">bolt</i>',
    htmlAfter: '',
  }, {
    title: 'home.temperature',
    htmlBefore: '<i class="material-icons">show_chart</i>',
    to: {
      name: 'blog-posts',
    },
  }, {
    title: 'home.river',
    htmlBefore: '<i class="material-icons">waves</i>',
    to: {
      name: 'add-new-post',
    },
  }, {
    title: 'home.vegetable_garden',
    htmlBefore: '<i class="material-icons">local_florist</i>',
    to: {
      name: 'components-overview',
    },
  }, {
    title: 'home.greenhouse',
    htmlBefore: '<i class="material-icons">home</i>',
    to: {
      name: 'tables',
    },
  }, {
    title: 'home.devices',
    htmlBefore: '<i class="material-icons">router</i>',
    to: {
      name: 'user-profile-lite',
    },
  }, {
    title: 'home.logs',
    htmlBefore: '<i class="material-icons">error</i>',
    to: {
      name: 'errors',
    },
  }];
}

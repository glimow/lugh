const axios = require('axios');

const sdk = {

  getProduction() {
    return axios.get(`${process.env.VUE_APP_API_URL}/production`)
      .then(res => res.data);
  },

  getSolarProduction() {
    return axios.get(`${process.env.VUE_APP_API_URL}/production/solar`)
      .then(res => res.data);
  },

  getWindProduction() {
    return axios.get(`${process.env.VUE_APP_API_URL}/production/wind`)
      .then(res => res.data);
  },

  getConsumption() {
    return axios.get(`${process.env.VUE_APP_API_URL}/consumption`)
      .then(res => res.data);
  },

  getBattery() {
    return axios.get(`${process.env.VUE_APP_API_URL}/battery`)
      .then(res => res.data);
  },

};
console.log(process.env);
export default sdk;

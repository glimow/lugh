# Lugh : Simple Home Energy Production Monitoring and Forecasting

![Lugh Screenshot](lugh.png)

## Stack

- Frontend: Vue
- Backend: Node
- ML: Python/Creme
- Sensor Driver : Python

## About

*Lugh* is a Celtic god of Sun, Art and Craftmanship, I thought it matched this
DIY, solar-powered project pretty well.

